/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    'GET /searchAnuncio': {
        controller: 'AnuncioController',
        action: 'getAnunciosByPalabra'
    },
    'POST /updatePersonsAnuncio': {
        controller: 'AnuncioController',
        action: 'updatePersons'
    },
    'POST /getPersonsInAnuncio': {
        controller: 'AnuncioController',
        action: 'getPersons'
    },
};