/**
 * RegistroPrimarioEmprs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
        nombreUserEmprs: {
            type: 'string',
            required: true,
            minLength: 3,
            maxLength: 30,
        },
        apellidoUserEmprs: {
            type: 'string',
            required: true,
            minLength: 5,
            maxLength: 30,
        },
        emailUserEmprs: {
            type: 'string',
            required: true,
            isEmail: true,
            unique: true,

        },
        autenticacion: {
            type: 'number',
            required: true,
        },
        passwUserEmprs: {
            required: true,
            minLength: 8,
            maxLength: 30,
            type: 'string',
            // custom: function(value) {
            //     // • confirma q es un string
            //     // • confirma q sea al menos 8 caracteres de largo
            //     // • que contenga al menos un numero
            //     // • que contenga al menos una letra
            //     return _.isString(value) && value.length >= 8 && value.match(/[a-z]/i) && value.match(/[0-9]/);
            // }
        },
        empresa: {
            collection: 'empresa',
            via: 'owner'
        }

        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};