/**
 * Empresa.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        nombreEmpr: {
            type: 'string',
            required: true,
            minLength: 3,
            maxLength: 50,
        },
        rifEmpr: {
            type: 'string',
            required: true,
            unique: true,
            maxLength: 15,
        },
        descEmpr: {
            type: 'string',
            required: true,

        },
        cantEmpleads: {
            type: 'string',
        },
        imgEmpr: {
            type: 'string',
        },
        direccEmpr: {
            type: 'string',
            required: true,
        },
        razonSocialEmpr: {
            type: 'string',
            required: true,
            unique: true,
        },
        ///////////////////////////////////////////////////////////////////////////////////
        owner: {
            model: 'RegistroPrimarioEmprs',
            unique: true
        },
        ///////////////////////////////////////////////////////////////////////////
        referencsDeReclutadrsEmpr: {
            collection: 'referencsDeReclutadrsEmpr',
            via: 'empresReclutdr'
        },
        ///////////////////////////////////////////////////////////////////////////
        anuncios: {
            collection: 'anuncio',
            via: 'owner'
        },
        analistas: {
            collection: 'analista',
            via: 'owner'
        },
        industrType: {
            collection: 'industrType',
            via: 'owner'
        },
        sectrType: {
            collection: 'sectrType',
            via: 'owner'
        }
        ///////////////////////////////////////////////////////////////////////////////////
        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};