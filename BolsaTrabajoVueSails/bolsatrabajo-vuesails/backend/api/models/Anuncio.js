/**
 * Anuncio.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
//una empresa puede tener muchos anuncios
module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        tituloAnunc: {
            type: 'string',
            required: true,
            unique: true,
            minLength: 15,
            maxLength: 144,
        },
        cantVacnts: {
            type: 'string',
            //required: true,
        },
        descrpAnunc: {
            type: 'string',
            required: true,
        },
        jerarquia: {
            type: 'string',
        },
        tipoempleo: {
            type: 'string',
        },
        modalidtrabj: {
            type: 'string',
        },
        ofertsalarl: {
            type: 'string',
            required: true,
        },
        pDiscapacitds: {
            type: 'string',
            required: true,
        },
        anuncEstado: {
            type: 'string',
            isIn: ['activo', 'inactivo', 'onHold'],
            defaultsTo: "activo"
        },
        fechFinalzcn: {
            type: 'string',
            required: true,
        },
        tipoAnunc: {
            type: 'string',
            isIn: ['normal', 'destacado', 'premium'],
            //required: false,
        },
        contactado: {
            type: 'string',
            isIn: ['si', 'no'],
            defaultsTo: "no"
        },
        ////////////////////////////////////////////////////////////////////////////
        createdByAnalist: {
            model: 'analista'
        },
        updatedByAnalist: {
            collection: 'analista', //AQUIII???//
            via: 'anunciosActualizds'
        },
        ////////////////////////////////////////////////////////////////////////////
        owner: {
            model: 'Empresa',
        },
        registroPrimarioUsers: {
            collection: 'registroPrimarioUser',
            via: 'anuncios'
        },
        ////////////////////////////////////////////////////////////////////////////
        postulacnAnuncios: {
            collection: 'postulacnAnuncio',
            via: 'owner'
        },
        ////////////////////////////////////////////////////////////////////////////
        requistAnunc: {
            collection: 'requistAnunc',
            via: 'owner'
        },
        ////////////////////////////////////////////////////////////////////////////
        localidadAnuncs: {
            collection: 'localidadAnunc',
            via: 'owner'
        },
        ////////////////////////////////////////////////////////////////////////////
        areaLaborals: {
            collection: 'areaLaboral',
            via: 'owner'
        },
        ////////////////////////////////////////////////////////////////////////////
        cargosAnuncs: {
            collection: 'cargosAnunc',
            via: 'owner'
        },
        ////////////////////////////////////////////////////////////////////////////
        evaluacns: {
            collection: 'evaluacnAnalist',
            via: 'anuncioOwner'
        }
        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    }

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

}