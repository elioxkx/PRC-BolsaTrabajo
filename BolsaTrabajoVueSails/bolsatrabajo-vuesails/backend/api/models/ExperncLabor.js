/**
 * ExperncLabor.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
        nombEmprs: {
            type: 'string',
            required: true,
        },
        idActvdEmprs: {
            type: 'string',
            required: true,
        },
        cargo: {
            type: 'string',
            required: true,
        },
        idPais: {
            type: 'string',
            required: true,
        },
        fechDesd: {
            type: 'string',
            required: true,
        },
        fechHast: {
            type: 'string',
            required: true,
        },
        alPresnt: {
            type: 'string',
            //required: true,
        },
        areaCargo: {
            type: 'string',
            required: true,
        },
        nivlExpernc: {
            type: 'string',
            required: true,
        },
        descrpRespnsabld: {
            type: 'string',
            required: true,
        },
        persnsaCargo: {
            type: 'number',
            required: true,
        },
        owner: {
            model: 'persona',
        }
        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};