/**
 * RegistroPrimarioUser.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        nombreUser: {
            type: 'string',
            required: true,
            minLength: 2,
            maxLength: 30,
        },
        apellidoUser: {
            type: 'string',
            required: true,
            minLength: 2,
            maxLength: 30,
        },
        emailUser: {
            type: 'string',
            required: true,
            isEmail: true,
            unique: true,
        },
        telf1: {
            type: 'string',
            //required: true,
            //unique: true,
        },
        autenticacion: {
            type: 'number',
            required: true,
        },
        passwUser: {
            required: true,
            minLength: 8,
            maxLength: 30,
            type: 'string',
            // custom: function(value) {
            //     // • confirma q es un string
            //     // • confirma q sea al menos 8 caracteres de largo
            //     // • que contenga al menos un numero
            //     // • que contenga al menos una letra
            //     return _.isString(value) && value.length >= 8 && value.match(/[a-z]/i) && value.match(/[0-9]/);
            // }
        },
        //////////////////////////////////////////////////////////////////////
        persona: {
            collection: 'persona',
            via: 'owner'
        },
        anuncios: {
            collection: 'anuncio',
            via: 'registroPrimarioUsers'
        },
        evaluacnAnalists: {
            collection: 'evaluacnAnalist',
            via: 'owner'
        },
        //////////////////////////////////////////////////////////////////////
        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};