/**
 * ReferncUser.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
        nombRefer: {
            type: 'string',
            required: true,
        },
        apellidRefer: {
            type: 'string',
            required: true,
        },
        emailRefer: {
            type: 'string',
            isEmail: true,
            required: true,
            unique: true
        },
        telefRefer: {
            type: 'string',
            required: true,
        },
        tipoRefer: {
            type: 'string',
            required: true,
        },
        relacnRefer: {
            type: 'string',
            required: true,
        },
        confirmRefer: {
            type: 'string',
            required: true,
        },
        owner: {
            model: 'persona',
        }

        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};