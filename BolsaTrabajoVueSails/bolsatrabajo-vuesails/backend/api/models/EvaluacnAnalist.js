/**
 * EvaluacnAnalist.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
        comment: {
            type: 'string',
            minLength: 10,
            maxLength: 200,
        },
        dataRevisd: {
            type: 'string',
            isIn: ['false', 'true', true, false],
        },
        dataEvaluacn: {
            type: 'string',
            isIn: ['false', 'true', true, false],
        },
        dataFaseFinal: {
            type: 'string',
            isIn: ['false', 'true', true, false],
            //required: true,
        },
        dataContctdo: {
            type: 'string',
            isIn: ['false', 'true', true, false],
            //required: true,
        },
        atr1: {
            type: 'string',
        },
        atr2: {
            type: 'string',
        },
        atr3: {
            type: 'string',
        },
        ///////////////////////////////////////////////////////////////////////////////////
        owner: {
            model: 'RegistroPrimarioUser',
            //unique: true
        },
        analist: {
            model: 'analista'
        },
        ///////////////////////////////////////////////////////////////////////////////////
        anuncioOwner: {
            model: 'anuncio'
        }
        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};