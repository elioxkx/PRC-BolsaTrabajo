/**
 * Analista.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        nombreAnalist: {
            type: 'string',
            required: true,
            minLength: 2,
            maxLength: 30,
        },
        apellidoAnalist: {
            type: 'string',
            required: true,
            minLength: 2,
            maxLength: 30,
        },
        emailAnalist: {
            type: 'string',
            required: true,
            isEmail: true,
            unique: true,
        },
        cargoAnalist: {
            type: 'string',
            isIn: ['admin', 'analista'],
            required: true,
            //unique: true,
        },
        estado: {
            type: 'string',
            isIn: ['activo', 'inactivo'],
            defaultsTo: 'activo'
                //unique: true,
        },
        passwAnalist: {
            type: 'string',
            required: true,
        },
        rolAnalist: {
            type: 'string',
            required: true,
            isIn: ['junior', 'senior', 'NA'],
            // custom: function(value) {
            //     // • confirma q es un string
            //     // • confirma q sea al menos 8 caracteres de largo
            //     // • que contenga al menos un numero
            //     // • que contenga al menos una letra
            //     return _.isString(value) && value.length >= 8 && value.match(/[a-z]/i) && value.match(/[0-9]/);
            // }
        },
        //////////////////////////////////////////////////////////////////////
        owner: {
            model: 'empresa',
            required: true,
        },
        evaluacns: {
            collection: 'evaluacnAnalist',
            via: 'analist'
        },
        analistaLogs: {
            collection: 'analistaLog',
            via: 'owner'
        },

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        anuncios: {
            collection: 'anuncio',
            via: 'createdByAnalist'
        },
        anunciosActualizds: {
            collection: 'anuncio',
            via: 'updatedByAnalist'
        },
        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};