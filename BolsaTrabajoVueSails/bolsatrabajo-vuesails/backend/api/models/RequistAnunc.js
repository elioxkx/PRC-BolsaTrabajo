/**
 * RequistAnunc.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        // sexoAnunc
        // edadMinAnunc
        // edadMaxAnunc
        // idioma
        // nivIdiom
        // nivEducativo
        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        sexoAnunc: {
            type: 'string',
            required: true,
        },
        edadMinAnunc: {
            type: 'string',
            required: true,
        },
        edadMaxAnunc: {
            type: 'string',
            required: true,
        },
        idioma: {
            type: 'number',
            required: true,
        },
        nivIdiom: {
            type: 'string',
            required: true,
        },
        nivEducativo: {
            type: 'string',
            required: true,
        },
        owner: {
            model: 'anuncio',
            unique: true
        }
        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};