/**
 * Persona.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        fech_nacimnt: {
            type: 'string',
        },
        genero: {
            type: 'string',
            required: true,
            maxLength: 15,
        },
        nacionalidad: {
            type: 'string',
            required: true,
        },
        tipoDocument: {
            type: 'string',
            required: true
        },
        nroDocument: {
            type: 'string',
            required: true,
            unique: true
        },
        discapacitad: {
            type: 'string',
            //required: true,
        },
        estadCivil: {
            type: 'string',
            //required: true,
        },
        telf2: {
            type: 'string',
        },
        direccion: {
            type: 'string',
            //required: true,
        },
        imgPerfil: {
            type: 'string',
        },
        currcVitArchiv: {
            type: 'string',
        },

        ////////////////////////////////////////////////////////////
        //////////////Tablas asociadas a Persona////////////////////
        ////////////////////////////////////////////////////////////

        ///Tabla dueño o padre:RegistroPrimarioUser//R:1->1//////////Tabla hij@:Registro de anuncios postulados con multiples dueños(anuncios) en entidad->postulacnAnuncios//R:1->N/////////////////
        owner: {
            model: 'RegistroPrimarioUser',
            //unique: true
        },
        postulacnAnuncios: {
            collection: 'postulacnAnuncio',
            via: 'owners'
        },
        ////////////Tabla hij@:Registro logueo del usuario//R:1->N///////
        personLogs: {
            collection: 'personLog',
            via: 'owner'
        },
        //////////////////////////////////

        ////////////Tabla hij@:Registro educación del usuario//R:1->N///////
        educacnUsers: {
            collection: 'educacnUser',
            via: 'owner'
        },
        //////////////////////////////////


        ////////////Tabla hij@:Registro referencias del usuario//R:1->N///////
        referncUsers: {
            collection: 'referncUser',
            via: 'owner'
        },
        //////////////////////////////////


        ////////////Tabla hij@:Registro experiencias laborales del usuario//R:1->N///////
        experncLabors: {
            collection: 'experncLabor',
            via: 'owner'
        },
        //////////////////////////////////


        ////////////Tabla hij@:Registro preferencia laboral del usuario//R:1->1///////
        preferncLabor: {
            collection: 'preferncLabor',
            via: 'owner'
        },
        //////////////////////////////////


        ////////////Tabla hij@:Registro localidad del usuario/////////
        localidad: {
            collection: 'localidad',
            via: 'owner'
        },
        //////////////////////////////////


        //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
        //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
        //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    },

};