/**
 * AnuncioController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

//const Anuncio = require("../models/Anuncio");

//const Anuncio = require("../models/Anuncio");

//const Anuncio = require("../models/Anuncios");

module.exports = {
    /**
     * `AnuncioController.get()` ${this.$store.state.regUserEmprs.empresa[0].id} `${}`
     */
    getAnunciosByPalabra: async(req, res) => {
        console.log("ta llegando ", req.param('palabra'));
        var anunciosBypalabra = await Anuncio.find({
            tituloAnunc: {
                'contains': `${req.param('palabra')}`,
                //'contains': 'sera',
                //'contains': req.param('treu')
            },
            //tituloAnunc: { contains: req.param('treu') }
        });
        console.log("data: ", anunciosBypalabra)

        // var anunciosBypalabraEspec = await Anuncio.find({
        //     tituloAnunc: {
        //         //'contains': 'ncio', 
        //         'contains': req.param('palabra')
        //     },
        //     //tituloAnunc: { contains: req.param('treu') }
        // });
        // console.log("data 2: ", anunciosBypalabraEspec)

        return res.json({

            anunciosBypalabra
            //anunciosBypalabraEspec,
            //todo: 'get() is not implemented yet!',
        });
    },
    updatePersons: async(req, res) => {
        // const result = await Anuncio.find({
        //     where: { id: '616467526ce32448047a620c' }
        // });
        //const result = await Anuncio.findOne({ id: req.body.id })

        await Anuncio.addToCollection(req.body.idAnunc, 'registroPrimarioUsers')
            .members([req.body.idPerson]);
        //console.log("HOLAA llega", req.body.idAnunc, req.body.idPerson)

        return res.status(200).json()
    },
    getPersons: async(req, res) => {
        // const result = await Anuncio.find({
        //     where: { id: '616467526ce32448047a620c' }
        // });
        //const result = await Anuncio.findOne({ id: req.body.id })

        // await Anuncio.addToCollection(req.body.idAnunc, 'personas')
        //     .members([req.body.idPerson]);
        console.log("HOLAA llega", req.body)

        return res.status(200).json(Anuncio.attributes)
    },

};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//**
//  * AnuncioController
//  *
//  * @description :: Server-side actions for handling incoming requests.
//  * @help        :: See https://sailsjs.com/docs/concepts/actions
//  */

// //const Anuncio = require("../models/Anuncio");

// //const Anuncio = require("../models/Anuncio");
// module.exports = {


//     /**
//      * `AnuncioController.create()`
//      */
//     updatePersons: async(req, res) => {
//         // const result = await Anuncio.find({
//         //     where: { id: '616467526ce32448047a620c' }
//         // });
//         //const result = await Anuncio.findOne({ id: req.body.id })

//         await Anuncio.addToCollection(req.body.idAnunc, 'personas')
//             .members([req.body.idPerson]);
//         console.log("HOLAA llega", req.body.idAnunc, req.body.idPerson)

//         return res.status(200).json()
//     },
//     getPersons: async(req, res) => {
//         // const result = await Anuncio.find({
//         //     where: { id: '616467526ce32448047a620c' }
//         // });
//         //const result = await Anuncio.findOne({ id: req.body.id })

//         // await Anuncio.addToCollection(req.body.idAnunc, 'personas')
//         //     .members([req.body.idPerson]);
//         var recordd = await Anuncio.find({
//             where: { owner: '6171cd9293d71812b9b0289b' },
//             select: ['estadCivil', 'tipoDocument']
//         });
//         console.log("sera que sipo", recordd)

//         console.log("HOLAA llega", req.body)
//         res.json(req.body)

//         return res.status(200).json()
//     },
//     create: async function(req, res) {
//         return res.json({
//             todo: 'create() is not implemented yet!'
//         });
//     },

//     /**
//      * `AnuncioController.delete()`
//      */
//     delete: async function(req, res) {
//         return res.json({
//             todo: 'delete() is not implemented yet!'
//         });
//     },

//     /**
//      * `AnuncioController.update()`
//      */
//     update: async function(req, res) {
//         return res.json({
//             todo: 'update() is not implemented yet!'
//         });
//     },

//     /**
//      * `AnuncioController.get()`
//      */
//     get: async function(req, res) {
//         return res.json({
//             todo: 'get() is not implemented yet!'
//         });
//     }

// };