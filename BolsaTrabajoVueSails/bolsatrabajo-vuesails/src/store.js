import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    // ...
    state: {
        regUser: {
            // nombreUser: "",
            // apellidoUser: "",
            // emailUser: "",
            //telf1: "",
            // autenticacion: "1",
            // passwUser: "",
            // id: null
        },
        regUserEmprs: {
            // nombreUserEmpres: "",
            // apellidoUserEmpres: "",
            // emailUserEmpres: "",
            // autenticacion: "1",
            // passwUserEmpres: "",
            // id: null
        },
        regEmprs: {
            // nombreEmpr: "",
            // rifEmpr: "",
            // descEmpr: "",
            // cantEmpleads: "",
            // direccEmpr: "",
            // razonSocialEmpr: "",
            // owner: null,
        },
        analista: {
            // nombreAnalist,
            // apellidoAnalist,
            // emailAnalist,
            // cargoAnalist,
            // estado,
            // passwAnalist,
            // rolAnalist,
            // owner,
            // evaluacns,
            // analistaLogs,
            // anuncios,
            // anunciosActualizds
        },
        userData: {
            // nombreUser: "",
            // apellidoUser: "",
            // emailUser: "",
            // id: null
        },
        anuncs: {
            // id:"615b3d80a2ddd613d647637a"
            // anuncActivo:true
            // updatedAt:1633369472550
            // tituloAnunc:"asistente atenccion al cliente"
            // owner:Object
            //     nombreEmpr:"empresa1"
            //     rifEmpr:"v12352"
            // updatedAt:1633369472550
            // descrpAnunc:"loremloremloremloremloremlorem"
            // tipoempleo:"Pasantia"
            // modalidtrabj:"Desde casa"
            // ofertsalarl:"12332"
            // cvemailcopy:"true"
        },
        anuncsByPalabra: {
            searchQuery: "",
            responseQuery: []
        },
        anuncsByEmpresa: {

        },
    },
    mutations: {
        getRegUser(state, payload) {
            state.regUser = payload
        },
        getRegUserEmprs(state, payload) {
            state.regUserEmprs = payload
        },
        getEmprs(state, payload) {
            state.regEmprs = payload
        },
        getAnalist(state, payload) {
            state.analista = payload
        },
        getAnuncs(state, payload) {
            state.anuncs = payload
        },
        getAnuncsByEmpresa(state, payload) {
            state.anuncsByEmpresa = payload
        },
        getAnuncsByPalabra(state, payload) {
            state.anuncsByPalabra.searchQuery = payload.searchQuery
            state.anuncsByPalabra.responseQuery = payload.responseQuery
        },
        getUserData(state, payload) {
            state.userData = payload
        }
    },
    getters: {},
    actions: {}
})
export default store