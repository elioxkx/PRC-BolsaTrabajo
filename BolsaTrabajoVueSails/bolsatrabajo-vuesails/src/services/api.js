const url = 'http://localhost:1337/anuncio'

function getAssets() {
    // return fetch(`${url}/assets?limit=20`)
    return fetch(`${url}`)
        .then(res => res.json())
        .then(res => res.data)
}

export default {
    getAssets
}