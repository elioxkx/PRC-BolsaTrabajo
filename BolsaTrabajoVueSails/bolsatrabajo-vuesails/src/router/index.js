import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home.vue";

import Home2 from "@/components/Home2";
import Empresa from "@/components/Empresa";
import Anuncio from "@/components/Anuncio";
import Exp_laboral from "@/components/Exp_laboral";
import Prf_labor from "@/components/Pref_labor";
import Ref_labor from "@/components/Ref_labor";
import Idiom from "@/components/Idiom";
import Contact from "@/components/Contact";
import DatPerson from "@/components/DatPerson";
import CuadroAnuncio from "@/components/CuadroAnuncio";
import IndexGlob from "@/components/IndexGlob";
import EmpresaReg from "@/components/EmpresaReg";
import AnunReg from "@/components/AnunReg";
import PersonReg from "@/components/PersonReg";
import PersonReg2 from "@/components/PersonReg2";
import Homec from "@/components/HomeCard";
import AnuncioCreado from "@/components/AnuncioCreado";
import cv from "@/components/cv";
import Educacion from "@/components/Educacion";
import login from "@/components/login";
import loginEmprs from "@/components/loginEmprs";
import RegistrarPerson from "@/components/RegistrarPerson";
import RegistrarEmprs from "@/components/RegistrarEmprs";
import HeaderHome from "@/components/HeaderHome";
import EmpleosHome from "@/components/EmpleosHome";
import AnuncioDataTable from "@/components/AnuncioDataTable";
import AnunciosLanding from "@/components/AnunciosLanding";
import AnunciosBusqueda from "@/components/AnunciosBusqueda";
import AnunciosbyEmprs from "@/components/AnunciosbyEmprs";
import AnunciosEditarByEmprs from "@/components/AnunciosEditarByEmprs";
import AnuncioPostulds from "@/components/AnuncioPostulds";
import PersonaAnuncios from "@/components/PersonaAnuncios";
import sideBarMenu from "@/components/sideBarMenu";
import pivot from "@/components/pivot";
import personPerfil from "@/components/personPerfil";
import personLandin from "@/components/personLandin";
import personDatos from "@/components/personDatos";
import personsAccordeon from "@/components/personsAccordeon";
import testComponent from "@/components/testComponent";
import anuncioListPostulads from "@/components/AnuncioListPostulads";
import analistaCreate from "@/components/AnalistaCreate";
import analistaConsultaTodos from "@/components/AnalistaConsultaTods";
import loginAnalista from "@/components/loginAnalista";
import homeAnalista from "@/components/HomeAnalista";




Vue.use(VueRouter);

const routes = [

    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/About.vue"),
    },
    {
        path: "/home2",
        name: "Home2",
        component: Home2,
    },
    {
        path: '/empresa',
        component: Empresa,
        name: 'Empresa',
        meta: {
            auth: true
        },
    },

    {
        path: '/anuncio',
        component: Anuncio,
        name: 'Anunciooo',

    },
    {
        path: '/testComponent',
        component: testComponent,
        name: 'testComponent',

    },


    {
        path: '/experiencia',
        component: Exp_laboral,
        name: 'exp_labor',

    },

    {
        path: '/prflaboral',
        component: Prf_labor,
        name: 'prf_labor',

    },

    {
        path: '/reflaboral',
        component: Ref_labor,
        name: 'ref_labor',

    },

    {
        path: '/idiom',
        component: Idiom,
        name: 'idiom',

    },

    {
        path: '/contact',
        component: Contact,
        name: 'contact',

    },

    {
        path: '/datoperson',
        component: DatPerson,
        name: 'dat_person',

    },

    {
        path: '/cuadroanuncio',
        component: CuadroAnuncio,
        name: 'cuadroanuncio',

    },

    {
        path: '/bench',
        component: IndexGlob,
        name: 'bench',

    },

    {
        path: '/empresaReg',
        component: EmpresaReg,
        name: 'empresareg',

    },

    {
        path: '/anunReg',
        component: AnunReg,
        name: 'anunreg',

    },

    {
        path: '/personReg',
        component: PersonReg,
        name: 'personreg',

    },

    {
        path: '/personReq',
        component: PersonReg2,
        name: 'personreg2',

    },

    {
        path: '/homec',
        component: Homec,
        name: 'homec',

    },

    {
        path: '/anucCreat',
        component: AnuncioCreado,
        name: 'AnuncioCreado',

    },
    {
        path: '/anunciosLanding',
        component: AnunciosLanding,
        name: 'AnunciosLanding',

    },
    {
        path: '/anunciosBusqueda',
        component: AnunciosBusqueda,
        name: 'AnunciosBusqueda',

    },

    {
        path: '/anunciosEditarByEmprs',
        component: AnunciosEditarByEmprs,
        name: 'AnunciosEditarByEmprs',

    },
    {

        path: '/datoscv',
        component: cv,
        name: 'DatosCv',
        meta: {
            auth: true
        },
    },
    {

        path: '/datosEducacion',
        component: Educacion,
        name: 'DatosEducacion',


    },
    {

        path: '/personsAccordeon',
        component: personsAccordeon,
        name: 'personsAccordeon',


    },
    {
        path: '/login',
        component: login,
        name: 'Login',
    },
    {
        path: '/pivot',
        component: pivot,
        name: 'pivot',
    },
    {
        path: '/loginEmprs',
        component: loginEmprs,
        name: 'loginEmprs',
    },
    {

        path: '/RegistroPerson',
        component: RegistrarPerson,
        name: 'RegistrarPerson',

    },
    {

        path: '/personPerfil',
        component: personPerfil,
        name: 'personPerfil',

    },
    {

        path: '/personLandin',
        component: personLandin,
        name: 'personLandin',

    },
    {

        path: '/RegistrarEmprs',
        component: RegistrarEmprs,
        name: 'RegistrarEmprs',

    },
    {

        path: '/HeaderHome',
        component: HeaderHome,
        name: 'HeaderHome',
    },
    {

        path: '/sideBarMenu',
        component: sideBarMenu,
        name: 'sideBarMenu',
    },

    {

        path: '/personDatos',
        component: personDatos,
        name: 'personDatos',
    },
    {

        path: '/EmpleosHome',
        component: EmpleosHome,
        name: 'EmpleosHome',


    },
    {

        path: '/anuncioDaTable',
        component: AnuncioDataTable,
        name: 'AnuncioDaTable',

    },
    {

        path: '/anunciosbyEmprs',
        component: AnunciosbyEmprs,
        name: 'AnunciosbyEmprs',

    },
    {

        path: '/anuncioPostulds',
        component: AnuncioPostulds,
        name: 'AnuncioPostulds',

    },
    {

        path: '/anuncioListPostulads',
        component: anuncioListPostulads,
        name: 'anuncioListPostulads',

    },
    {

        path: '/analistaCreate',
        component: analistaCreate,
        name: 'AnalistaCrear',

    },

    {

        path: '/analistaConsultAll',
        component: analistaConsultaTodos,
        name: 'AnalistaConsultaTodos',

    },
    {

        path: '/personaAnuncios',
        component: PersonaAnuncios,
        name: 'PersonaAnuncios',

    },
    {

        path: '/loginAnalista',
        component: loginAnalista,
        name: 'loginAnalista',

    },
    {

        path: '/homeAnalista',
        component: homeAnalista,
        name: 'homeAnalista',

    }



];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;