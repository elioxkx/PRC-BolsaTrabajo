import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store"
import '@/assets/css/tailwind.css'

import ReadMore from 'vue-read-more';
import Notifications from "vt-notifications";

Vue.use(ReadMore);
Vue.use(Notifications);


Vue.config.productionTip = false;

new Vue({
    router,
    render: (h) => h(App),
    store
}).$mount("#app");